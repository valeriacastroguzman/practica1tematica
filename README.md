# Paso de mensajes
## Comunicación
<<<<<<< HEAD
Para la comunicación entre el cliente y el servidor se utilizara
un standard de separadores para realizar el manejo de los eventos

##### Los separadores estaran distribuidos de la forma:
```
{identifier}<SEPARATOR>{action}
```

Esta expreción es checkeada con la siguiente expresión regular

```regexp
([a-z])+(<SEPARATOR>)((([a-z])+(<SEPARATOR>)([a-z]|[A-Z])+)+|[a-z]+)
``` 

## Buckets
Para el manejo de los buckets se utilzara los siguientes protocolos
para el cambio de mensaje

ATRAVEZ DEL SOCKET ESTO SERA ENVIANDO EN LENGUAJE BINARIO

#### Añadir
Se debe enviar la siguiente petición
```
bucket<SEPARATOR>add<SEPARATOR>{bucket_name}
``` 
Lugo se envia el nombre del bucket por ultimo el servidor enviara
una respuesta

#### Listar
```
bucket<SEPARATOR>list
```

El servidor respondera los buckets
disponibles separados por comas en el ultimo parametro 
de los separadores

```
bucket<SEPARATOR>list<SEPARATOR>"archivos","universidad"
```

#### Eliminar
Se debe enviar el siguiente petición
```
bucket<SEPARATOR>delete<SEPARATOR>{bucket_name}
```
Luego el servidor dara una respuesta acerca del resultado de una
transacción.

#### Cambiar nombre
Se debe enviar el siguiente comando.
```
bucket<SEPARATOR>update_name
```
El servidor respondera un received para que luego
se envie la información del bucket de la siguiente
manera
```
{old_name}<SEPARATOR>{new_name}
```

### Manejo de archivos.
Para realizar el manejo de archivos se tiene que cumplir el
siguiente protocolo de mensajes

#### Añadir archivo
Al iniciar se debe especificar la creación de un nuevo archivo
de la siguiente forma.
```
file<SEPARATOR>add
```

Luego de realizar la petición se debe enviar la información del
archivo así como el bucket de destino.
```
{filename}<SEPARATOR>{filesize}<SEPARATOR>{bucket_name}
```

Para terminar se debe enviar los bits del archivo.

#### Listar archivos
Para listar los archivos se debe enviar el siguiente comando
```
file<SEPARATOR>ls
```
Para terminar se debe enviar el nombre del bucket a listar.

#### Borrar archivo
Para eliminar un bucket se debe realizar la siguiente acción
```
file<SEPARATOR>rm<SEPARATOR>{bucket_name}
```
Por ultimo envia el nombre del archivo. Luego el servidor respondera
con el resultado de la transacción

#### Cambiar nombre
Para cambiar el nombre a un archivo se debe enviar la siguiente
petición
```
file<SEPARATOR>rename<SEPARATOR>{bucket}
```
Luego el servidor devolvera una confirmación `received` una vez recivido
se debe enviar el siguiente comando:
```
{old_name}<SEPARATOR>{new_name}
```

Por ultimo el servidor enviara una respuesta de la transacción

#### Descargar archivo
Para descargar un archivo se debe seguir el siguiente protocolo
```
file<SEPARATOR>download<SEPARATOR>{bucket_name}
```
Luego se enviar el nombre del archivo. Por ultumo el servidor 
respondera con los datos del archivo. 
