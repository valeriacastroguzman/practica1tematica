import { Component, Inject } from "@angular/core";
import { FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { codes, WebsocketService } from 'src/services/websocket.service';


@Component({
    templateUrl: 'administrator.component.html',
    styleUrls: ['administrator.component.scss'],
    providers: [WebsocketService]
})
export class AdministradorComponent {
    public buckets: string[] = [];
    public newBucketName = new FormControl('', []);
    public selectedBucket = "Selecciona un bucket..";
    public filesInBucket = [];
    private fileReader = new FileReader();
    private readedFiles = [];
    private sendFile = null;

    constructor(public websocketService: WebsocketService, public dialog: MatDialog) {
        websocketService.connect().subscribe(result => {
            console.log(result);
            const data = JSON.parse(result.data);
            if (data.code == "connect") {
                this.loadBuckets();
            }
            this.actions(data);
        });
    }

    private actions(data) {
        if (data.code.includes("bucket")) {
            this.buckets = data.body.buckets;
        }
        else if (data.code.includes("file")) {
            if (data.code.split("_")[1] == "ready") {
                this.websocketService.sendMessage(codes.add_file, {
                    bucketName: this.selectedBucket,
                    fileName: this.sendFile.name,
                    hash: data.body.hash
                })
            }
            else {
                this.filesInBucket = data.body.files
            }
        }
        console.log(this.websocketService.socket.bufferedAmount);
    }

    public addBucket() {
        if (this.newBucketName.value == "") return;

        this.websocketService.sendMessage(codes.create_bucket, {
            name: this.newBucketName.value
        });
        this.newBucketName.setValue("");
    }

    public selectBucket(name: string) {
        this.selectedBucket = name;
        this.websocketService.sendMessage(codes.list_files, {
            bucketName: name
        });
    }

    public removeBucket(name: string) {
        if (name == "") return;

        this.websocketService.sendMessage(codes.delete_bucket, {
            name: name
        })
    }

    public onLoadInputChange(event: Event) {
        let files = event.target['files'];
        console.log(event)

        if (files) {
            const dialogRef = this.dialog.open(ConfirmFileDialog, {
                width: '500px',
                data: files
            });

            dialogRef.afterClosed().subscribe(response => {
                if (response)
                    this.readFiles(files, 0);
            })
        }
    }

    private readFiles(files: any[], index: number) {
        console.log(files[index] instanceof Blob)

        this.sendFile = files[index];
        this.websocketService.socket.send(files[index]);

        // this.fileReader.onload = () => {
        //     this.readedFiles.push(this.fileReader.result);

        //     console.log(this.fileReader.result);
        //     console.log(btoa(""+this.fileReader.result))


        //     this.websocketService.sendMessage(codes.add_file, {
        //         bucketName: this.selectedBucket,
        //         fileName: files[index].name,
        //         bytes: btoa(""+this.fileReader.result)
        //     })

        //     if (files[index + 1]) {
        //         this.readFiles(files, index + 1);
        //     }
        // }

        // this.fileReader.readAsBinaryString(files[index]);
    }

    private async loadBuckets() {
        this.websocketService.sendMessage(codes.list_buckets, {});
    }
}

@Component({
    selector: 'confirm_files',
    templateUrl: 'confirmFile.dialog.html',
    styleUrls: ['confirmFile.dialog.scss']
})
export class ConfirmFileDialog {
    constructor(
        public dialogRef: MatDialogRef<ConfirmFileDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    onNoClick() {
        this.dialogRef.close(false);
    }
}
